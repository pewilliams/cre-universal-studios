 ########
###Q4 - using Q415 data
##universal p18-34 target
library(data.table)
library(parallel)
library(reshape2)
library(ggplot2)


#static data inputs
uwtg <- readRDS('data/unifiedWeights.rds')
ndat <- readRDS('data/networkData.rds')
ndat <- transform(ndat, hhkey = paste0(broadcastdate,'-',hh), broadcastdate = as.IDate(broadcastdate,'%Y-%m-%d'))
ndat <- transform(ndat, stkey = paste0(networkcode, '-',sellingtitleid))

# scenarios - deal with proxy selling titles
# sdat <- read.csv('data/scenariosUnitCounts.csv',header=T,stringsAsFactors=F)
# sdat <- data.table(sdat)
# sdat <- transform(sdat, stkey = as.character(paste0(networkcode, '-', sellingtitleid)))
# setdiff(unique(sdat$stkey), unique(ndat$stkey)) #in scenarios - but not in historical data. 
# sdat <- transform(sdat, ptkey = ifelse(stkey %in% unique(ndat$stkey), stkey, NA))

#scenarios with proxies
sdat <- read.csv('data/scenariosWithProxies.csv',header=T,stringsAsFactors=F) 
sdat <- data.table(sdat)


# scenario 5 = 2a, 6 = 2b. 
# s56 <- sdat[scenario %in% 5:6] 
# allts <- unique(ndat$stkey)
# titles <- unique(s56$ptkey)
# titles[titles %in% allts]


# SAMPLING FUNCTION - takes a selling title and unit count and gets respondent level data back - 
# must apply across all inputs - and then replicate for simulations
getRspDataSingleInput <- function(ST,unitCount, minData){ #get the respondent minute level data for a random minute within a random half hour
  hhBuckets <- unique(minData[stkey == ST]$hhkey);
  if(length(hhBuckets) < unitCount){ 
    hhBuckets <- sample(hhBuckets, unitCount, replace=T) #replace if the unit count is greater than possible placements
  }else{ 
    hhBuckets <- sample(hhBuckets, unitCount, replace=F)
  }
  rbindlist(lapply(hhBuckets, function(x) {
    sampleMin <- sample(x = minData[hhkey == x]$broadcastminute, size = 1, replace = F);
    minData[hhkey == x & broadcastminute == sampleMin]
    }))
}


# test inputs
# 
# mindata <- ndat[stkey %in% titles] 
# 
# #build respondent data for a single simulation
# scheduleData <- rbindlist(lapply(1:length(titles), function(a) getRspDataSingleInput(ST=titles[a],unitCount=unitCounts[a], minData = mindata)))

# length(unique(scheduleData$hhkey)) # number of placements selected

#AGGREGATION OF Schedule level data
#this is the big boring summary of the data - make sure to rbind all minute data from the selling title level inputs
#using program viewing weights for impressions, and unified quarterly weights for reach imps
#make sure network date is subset to demo of interest

# scheduleSummarise <- function(respondentdata, reachweights, SID){
#   #DAILY WEIGHTING SUMMARY  
#   #SCHEDULE IMPRESSIONS BY UNIT
#   scheduleAA <- respondentdata[,list(AA=sum(viewingweight)),
#   								by=list(broadcastminute,broadcastdate,networkcode,sellingtitlemaster,hhkey)]
#   scheduleAA <- scheduleAA[,list(unitcount=.N, gAA=sum(AA), flight = as.numeric(max(broadcastdate) - min(broadcastdate))), by = list(sellingtitlemaster)]
# 
#   scheduleAA <- dcast.data.table(scheduleAA, .~sellingtitlemaster, value.var = c('unitcount','gAA','flight'))
#   scheduleAA$. <- NULL;
#   
#   #build out the schedule unit and GRP summary  
#   #total schedule ReachAA (quarterly)
#   allrsp <- unique(respondentdata$personkey)
#   reachweights <- reachweights[personkey %in% allrsp]
#   reachAA <- sum(reachweights$wtg)
# 
#   #mary schedule stats to selling title level stats, and output
#   scheduleAA <- transform(scheduleAA, reachAA = reachAA, id = SID)
#   scheduleAA
# }

# test summary
# a <- scheduleSummarise(respondentdata = scheduleData, reachweights = uwtg, SID=1)


scheduleSummarise <- function(respondentdata, reachweights, SID){ #
  #DAILY WEIGHTING SUMMARY
  
  #SCHEDULE IMPRESSIONS BY UNIT
  scheduleAA <- respondentdata[,list(demoAA=sum(viewingweight)),
  								by=list(broadcastminute,broadcastdate,networkcode,stkey,hhkey)]
  scheduleAA <- scheduleAA[,list(unitcount=.N, gAA=sum(demoAA), flight = as.numeric(max(broadcastdate) - min(broadcastdate))), by = list(stkey)]

  scheduleAA <- dcast.data.table(scheduleAA, .~stkey, value.var = c('unitcount','gAA','flight'))
  #scheduleAA$. <- NULL;
  
  #build out the schedule unit and GRP summary
  
  #total schedule ReachAA (quarterly)
  allrsp <- unique(respondentdata$personkey)
  reachweights <- reachweights[personkey %in% allrsp]
  reachAA <- sum(reachweights$wtg)

  #mary schedule stats to selling title level stats, and output
  scheduleAA <- transform(scheduleAA, reachAA = reachAA, id = SID)
  
  #output by network
  netlist <- split(respondentdata, respondentdata$networkcode)
  reachsum <- lapply(netlist, function(a){
	rsplist <- unique(a$personkey)
	sum(reachweights[personkey %in% rsplist]$wtg)
})
   as.data.frame(c(reachsum,list('Total'=reachAA)) )
}



#scenario 1
set.seed(123)
numSims <- 100
scenario <- sdat[scenario == 1]
unitCounts <- scenario$units
titles <- scenario$ptkey
mindata <- ndat[stkey %in% titles] 

#proxy elf on shelf for people of earth, family guy for search part & separation
#were the millers for tammy

#sampling unit frame
otest <- 
    mclapply(1:numSims, function(x) {
    #selling title apply
    scheduleData <- rbindlist(lapply(1:length(titles), function(a) 
    getRspDataSingleInput(ST=titles[a],unitCount=unitCounts[a], minData = mindata))) 
    #get counts from x'th inputCount and the a'th selling title in the sequence
    scheduleSummarise(respondentdata = scheduleData, reachweights=uwtg, SID = x)
    },mc.cores=4)

outdat <- rbindlist(otest)
#setnames(outdat, names(outdat), make.names(names(outdat)))


s1 <- outdat





ptime <- proc.time()
#scenario 2
set.seed(123)
numSims <- 100
scenario <- sdat[scenario == 2]
unitCounts <- scenario$units
titles <- scenario$ptkey
mindata <- ndat[stkey %in% titles] 

#proxy elf on shelf for people of earth, family guy for search part & separation
#were the millers for tammy

#sampling unit frame
otest <- 
    mclapply(1:numSims, function(x) {
    #selling title apply
    scheduleData <- rbindlist(lapply(1:length(titles), function(a) 
    getRspDataSingleInput(ST=titles[a],unitCount=unitCounts[a], minData = mindata))) 
    #get counts from x'th inputCount and the a'th selling title in the sequence
    scheduleSummarise(respondentdata = scheduleData, reachweights=uwtg, SID = x)
    },mc.cores=10)

outdat <- rbindlist(otest)
#setnames(outdat, names(outdat), make.names(names(outdat)))

s2 <- outdat

proc.time() - ptime

#scenario 3
set.seed(123)
numSims <- 100
scenario <- sdat[scenario == 3]
unitCounts <- scenario$units
titles <- scenario$ptkey
mindata <- ndat[stkey %in% titles] 

#proxy elf on shelf for people of earth, family guy for search part & separation
#were the millers for tammy

#sampling unit frame
otest <- 
    mclapply(1:numSims, function(x) {
    #selling title apply
    scheduleData <- rbindlist(lapply(1:length(titles), function(a) 
    getRspDataSingleInput(ST=titles[a],unitCount=unitCounts[a], minData = mindata))) 
    #get counts from x'th inputCount and the a'th selling title in the sequence
    scheduleSummarise(respondentdata = scheduleData, reachweights=uwtg, SID = x)
    },mc.cores=10)

outdat <- rbindlist(otest)
#setnames(outdat, names(outdat), make.names(names(outdat)))

s3 <- outdat


#scenario 4
set.seed(123)
numSims <- 100
scenario <- sdat[scenario == 4]
unitCounts <- scenario$units
titles <- scenario$ptkey
mindata <- ndat[stkey %in% titles] 

#proxy elf on shelf for people of earth, family guy for search part & separation
#were the millers for tammy

#sampling unit frame
otest <- 
    mclapply(1:numSims, function(x) {
    #selling title apply
    scheduleData <- rbindlist(lapply(1:length(titles), function(a) 
    getRspDataSingleInput(ST=titles[a],unitCount=unitCounts[a], minData = mindata))) 
    #get counts from x'th inputCount and the a'th selling title in the sequence
    scheduleSummarise(respondentdata = scheduleData, reachweights=uwtg, SID = x)
    },mc.cores=10)

outdat <- rbindlist(otest)
#setnames(outdat, names(outdat), make.names(names(outdat)))

s4 <- outdat



#scenario 5 - 2a
set.seed(123)
numSims <- 50
scenario <- sdat[scenario == 5]
unitCounts <- scenario$units
titles <- scenario$ptkey
mindata <- ndat[stkey %in% titles] 

#proxy elf on shelf for people of earth, family guy for search part & separation
#were the millers for tammy

#sampling unit frame
otest <- 
    mclapply(1:numSims, function(x) {
    #selling title apply
    scheduleData <- rbindlist(lapply(1:length(titles), function(a) 
    getRspDataSingleInput(ST=titles[a],unitCount=unitCounts[a], minData = mindata))) 
    #get counts from x'th inputCount and the a'th selling title in the sequence
    scheduleSummarise(respondentdata = scheduleData, reachweights=uwtg, SID = x)
    },mc.cores=4)

outdat <- rbindlist(otest)
#setnames(outdat, names(outdat), make.names(names(outdat)))

s5 <- outdat


#scenario 6 - 2b
set.seed(123)
numSims <- 50
scenario <- sdat[scenario == 6]
unitCounts <- scenario$units
titles <- scenario$ptkey
mindata <- ndat[stkey %in% titles] 

#proxy elf on shelf for people of earth, family guy for search part & separation
#were the millers for tammy

#sampling unit frame
otest <- 
    mclapply(1:numSims, function(x) {
    #selling title apply
    scheduleData <- rbindlist(lapply(1:length(titles), function(a) 
    getRspDataSingleInput(ST=titles[a],unitCount=unitCounts[a], minData = mindata))) 
    #get counts from x'th inputCount and the a'th selling title in the sequence
    scheduleSummarise(respondentdata = scheduleData, reachweights=uwtg, SID = x)
    },mc.cores=4)

outdat <- rbindlist(otest)
#setnames(outdat, names(outdat), make.names(names(outdat)))

s6 <- outdat


#scenario 7
set.seed(123)
numSims <- 50
scenario <- sdat[scenario == 7]
unitCounts <- scenario$units
titles <- scenario$ptkey
mindata <- ndat[stkey %in% titles] 

#proxy elf on shelf for people of earth, family guy for search part & separation
#were the millers for tammy

#sampling unit frame
otest <- 
    mclapply(1:numSims, function(x) {
    #selling title apply
    scheduleData <- rbindlist(lapply(1:length(titles), function(a) 
    getRspDataSingleInput(ST=titles[a],unitCount=unitCounts[a], minData = mindata))) 
    #get counts from x'th inputCount and the a'th selling title in the sequence
    scheduleSummarise(respondentdata = scheduleData, reachweights=uwtg, SID = x)
    },mc.cores=10)

outdat <- rbindlist(otest)
#setnames(outdat, names(outdat), make.names(names(outdat)))

s7 <- outdat



#scenario 8
set.seed(123)
numSims <- 50
scenario <- sdat[scenario == 8]
unitCounts <- scenario$units
titles <- scenario$ptkey
#intersect(titles, unique(ndat$stkey))
mindata <- ndat[stkey %in% titles] 

#proxy elf on shelf for people of earth, family guy for search part & separation
#were the millers for tammy

#sampling unit frame
otest <- 
    mclapply(1:numSims, function(x) {
    #selling title apply
    scheduleData <- rbindlist(lapply(1:length(titles), function(a) 
    getRspDataSingleInput(ST=titles[a],unitCount=unitCounts[a], minData = mindata))) 
    #get counts from x'th inputCount and the a'th selling title in the sequence
    scheduleSummarise(respondentdata = scheduleData, reachweights=uwtg, SID = x)
    },mc.cores=12)

outdat <- rbindlist(otest)
#setnames(outdat, names(outdat), make.names(names(outdat)))

s8 <- outdat


#scenario 9
set.seed(123)
numSims <- 50
scenario <- sdat[scenario == 9]
unitCounts <- scenario$units
titles <- scenario$ptkey
#intersect(titles, unique(ndat$stkey))
mindata <- ndat[stkey %in% titles] 

#proxy elf on shelf for people of earth, family guy for search part & separation
#were the millers for tammy

#sampling unit frame
otest <- 
    mclapply(1:numSims, function(x) {
    #selling title apply
    scheduleData <- rbindlist(lapply(1:length(titles), function(a) 
    getRspDataSingleInput(ST=titles[a],unitCount=unitCounts[a], minData = mindata))) 
    #get counts from x'th inputCount and the a'th selling title in the sequence
    scheduleSummarise(respondentdata = scheduleData, reachweights=uwtg, SID = x)
    },mc.cores=12)

outdat <- rbindlist(otest)
#setnames(outdat, names(outdat), make.names(names(outdat)))

s9 <- outdat



#scenario 10
set.seed(123)
numSims <- 50
scenario <- sdat[scenario == 10]
unitCounts <- scenario$units
titles <- scenario$ptkey
#setdiff(titles, unique(ndat$stkey))
mindata <- ndat[stkey %in% titles] 

#proxy elf on shelf for people of earth, family guy for search part & separation
#were the millers for tammy

#sampling unit frame
otest <- 
    mclapply(1:numSims, function(x) {
    #selling title apply
    scheduleData <- rbindlist(lapply(1:length(titles), function(a) 
    getRspDataSingleInput(ST=titles[a],unitCount=unitCounts[a], minData = mindata))) 
    #get counts from x'th inputCount and the a'th selling title in the sequence
    scheduleSummarise(respondentdata = scheduleData, reachweights=uwtg, SID = x)
    },mc.cores=12)

outdat <- rbindlist(otest)
#setnames(outdat, names(outdat), make.names(names(outdat)))

s10 <- outdat











####updates for 

output <- suppressWarnings(data.frame(do.call('rbind',list(
apply(s1, 2, function(a) quantile(a, probs=c(0.5,0.05,0.95))),
apply(s2, 2, function(a) quantile(a, probs=c(0.5,0.05,0.95))),
apply(s3, 2, function(a) quantile(a, probs=c(0.5,0.05,0.95))),
apply(s4, 2, function(a) quantile(a, probs=c(0.5,0.05,0.95)))
))))

output$scenario <- rep(1:4, each = 3)
output$q <- rep(c('median','5th','95th'),4)


output2 <- suppressWarnings(data.frame(do.call('rbind',list(
apply(s5, 2, function(a) quantile(a, probs=c(0.5,0.05,0.95))),
apply(s6, 2, function(a) quantile(a, probs=c(0.5,0.05,0.95)))
))))

output$scenario <- rep(c('2a','2b', each = 3)
output$q <- rep(c('median','5th','95th'),4)



output3 <- suppressWarnings(data.frame(do.call('rbind',list(
apply(s7, 2, function(a) quantile(a, probs=c(0.5,0.05,0.95))),
apply(s8, 2, function(a) quantile(a, probs=c(0.5,0.05,0.95))),
apply(s9, 2, function(a) quantile(a, probs=c(0.5,0.05,0.95))),
apply(s10, 2, function(a) quantile(a, probs=c(0.5,0.05,0.95)))
))))



output3$scenario <- rep(c(7:10), each = 3)
output3$q <- rep(c('median','5th','95th'),4)

write.csv(output3, file = '/Users/pewilliams/Desktop/output3.csv',row.names=F)








