#########Data setup for CRE for Universal Studios....

dyn.load(Sys.getenv('JVM_DYLIB'))
options(java.parameters='-Xmx4g') # 4g

#these two libraries are required
library(RJDBC); library(data.table)

drv <- JDBC("net.sourceforge.jtds.jdbc.Driver", "/Applications/RazorSQL.app/Contents/Java/drivers/jtds/jtds12.jar", "'")
#initate connection to database, driver, host, user, password in that order - use global environ
conn <- dbConnect(drv, Sys.getenv('JDBC_EMERALD'), Sys.getenv('JDBC_USER'), Sys.getenv('JDBC_PWD'))
#get unified quarterly weights
uquery <- 
"select
rw.HouseholdNumber,
rw.PersonId,
r.Age,
r.Gender,
rw.ReportDate,
rw.UnifiedQuarterlyWeight

into #intab

from 
Emerald.dbo.RespondentWeights rw

inner join Emerald.dbo.Respondents r
on 
r._HouseholdId = rw._HouseholdId
and
r.PersonID = rw.PersonId

where
rw.ReportDate between '2015-09-28' and '2015-12-27'
and
rw.UnifiedQuarterlyWeight > 0

select
i.*
from #intab i
inner join
(select HouseholdNumber, PersonId,  max(ReportDate) maxDate from #intab
	group by HouseholdNumber, PersonId ) lw
on
lw.HouseholdNumber = i.HouseholdNumber
and
lw.PersonId = i.PersonId
and
lw.maxDate = i.ReportDate
order by 
i.HouseholdNumber,
i.PersonId asc

drop table #intab"


res <- dbSendQuery(conn, uquery) #send query 
uwtg <- fetch(res, n = -1)
uwtg <- data.table(uwtg)

dbClearResult(res) #clear result set to manage memory
dbDisconnect(conn) #close database connection

uwtg <- transform(uwtg, personkey = paste0(HouseholdNumber,'-',PersonId))

setnames(uwtg, names(uwtg), c('householdnumber','personid','age','gender','date','unifiedweight','personkey'))

saveRDS(uwtg, file ='data/uwtg.rds')


#get the network data you need
drv <- JDBC("net.sourceforge.jtds.jdbc.Driver", "/Applications/RazorSQL.app/Contents/Java/drivers/jtds/jtds12.jar", "'")
#initate connection to database, driver, host, user, password in that order - use global environ
conn <- dbConnect(drv, Sys.getenv('JDBC_REACH'), Sys.getenv('JDBC_USER'), Sys.getenv('JDBC_PWD'))

pquery <- "select
r.personkey
, r.age
, r.householdnumber
, r.personid
, n.networkcode
, dt.broadcastdate
, rvm.broadcastminute
, r.gender
, rv.viewingweight
, rv.intabweight
, rv.adjustmentfactor
, p.sellingtitleid
, p.sellingtitle
, p.sellingtitlemasterid
, p.sellingtitlemaster
from respondentprogramviewing rv
inner join refrespondent r
on rv._respondentsk = r._respondentsk
 
inner join refDate dt
on rv._datesk = dt._datesk
 
inner join refprogramairing p
on rv._programairingsk = p._programairingsk
inner join refnetwork n
on p._networksk = n._networksk
 
inner join bridgeRespondentProgramViewingMinute rvm with (nolock)
on rv._respondentprogramviewingsk = rvm._respondentprogramviewingsk
--and rvm.iscommercial = 1
 
inner join bridgeProgramairingMinute pam
on rvm._programairingsk = pam._programairingsk
and rvm.broadcastminute = pam.broadcastminute
--and pam.iscommercial = 1
 
where
dt._datesk between 20150928 and 20151227
and n.networkcode in ('TNT','TBSC','CNN','HLN','ADSM','TRU','NBAT','TOON');"

res <- dbSendQuery(conn, pquery) #send query 
pdat <- fetch(res, n = -1)
pdat <- data.table(pdat)

dbClearResult(res) #clear result set to manage memory
dbDisconnect(conn) #close database connection

saveRDS(pdat, file = 'data/pdat.rds')




ndat <- readRDS('data/pdat.rds')
ndat <- data.table(ndat)

#some filtering
ndat <- ndat[age > 17 & age < 35]
#ndat$adjustmentfactor[is.na(ndat$adjustmentfactor)] <- 1
ndat <- transform(ndat, hh = ceiling(broadcastminute/30))




#ue massaging stuff
drv <- JDBC("net.sourceforge.jtds.jdbc.Driver", "/Applications/RazorSQL.app/Contents/Java/drivers/jtds/jtds12.jar", "'")
#initate connection to database, driver, host, user, password in that order - use global environ
conn <- dbConnect(drv, Sys.getenv('JDBC_GENESIS'), Sys.getenv('JDBC_USER'), Sys.getenv('JDBC_PWD'))

ueQuery <- 
sprintf('
declare @startdate datetime = %s;
declare @enddate datetime = %s;

select
count(*) n,
ue."F18-20_NAT" + ue."M18-20_NAT" + ue."F21-24_NAT" + ue."M21-24_NAT" + ue."F25-29_NAT" + ue."M25-29_NAT" + ue."F30-34_NAT" + ue."M30-34_NAT" P1834UE,
ue.StartAiringDate,
ue.EndAiringDate

from 
facUniverseEstimate ue

where 
ue.StartAiringDate >= @startdate
and
ue.EndAiringDate <= @enddate

group by 
ue."F18-20_NAT" + ue."M18-20_NAT" + ue."F21-24_NAT" + ue."M21-24_NAT" + ue."F25-29_NAT" + ue."M25-29_NAT" + ue."F30-34_NAT" + ue."M30-34_NAT",
ue.StartAiringDate,
ue.EndAiringDate

order by 
ue.StartAiringDate asc', "'2015-09-28'","'2015-12-27'")

res <- dbSendQuery(conn, ueQuery) #send query 
uedat <- fetch(res, n = -1)
uedat <- data.table(uedat)

dbClearResult(res) #clear result set to manage memory
dbDisconnect(conn) #close database connection
uedat <- data.table(uedat)


p1834ue <- mean(uedat$P1834UE)  #average weekly - should be the same across weeks

uwtg <- uwtg[age > 17 & age < 35]
rawWtg <- with(uwtg, sum(unifiedweight))
adjFactor <- p1834ue*1e03/rawWtg
uwtg <- transform(uwtg, wtg = unifiedweight * adjFactor)


saveRDS(uwtg, file = 'data/unifiedWeights.rds')
saveRDS(ndat, file = 'data/networkData.rds')








